# Instalacja

composer install

uzupełnienie danych do połączenia się z bazą danych w .env

php bin/console doctrine:database:create

php bin/console doctrine:migrations:migrate

php bin/console doctrine:fixtures:load

# Uruchomienie

php -S 127.0.0.1:8000 -t public