<?php

namespace App\Repository;

use App\Entity\Item;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

use App\Utils\Filter;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Item::class);
    }

    /**
     * @return Item[] Returns an array of Item objects
     */
    public function findByAmount($amountFilter = null)
    {
        if ($amountFilter == null){
            return $this->findAll();
        }
        
        $amountFilter = Filter::parse($amountFilter);
        
        $queryBuilder = $this->createQueryBuilder('i');
        
        // to można przemyśleć czy dałoby radę zrobić uniwersalne, 
        // w zadaniu nie jest to wymagane
        if ($amountFilter[0] == Filter::EQUAL) {
            $queryBuilder->where('i.amount = :amount')
                         ->setParameter(':amount', $amountFilter[1]);
        } elseif ($amountFilter[0] == Filter::GREATER_THAN) {
            $queryBuilder->where('i.amount > :amount')
                         ->setParameter(':amount', $amountFilter[1]);
        }
        // inne, nie robiłem bo w zadaniu nie jest to potrzebne
        
        return $queryBuilder->getQuery()->getArrayResult();
    }

    /*
    public function findOneBySomeField($value): ?Item
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
