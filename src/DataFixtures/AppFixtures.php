<?php 
namespace App\DataFixtures;

use App\Entity\Item;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $items = [
            [
                'name' => 'Produkt 1',
                'amount' => '4',
            ],
            [
                'name' => 'Produkt 2',
                'amount' => '12',
            ],
            [
                'name' => 'Produkt 5',
                'amount' => '0',
            ],
            [
                'name' => 'Produkt 7',
                'amount' => '6',
            ],
            [
                'name' => 'Produkt 8',
                'amount' => '2',
            ],
        ];
        
        foreach ($items as $item) {
            $itemEntity = new Item();
            $itemEntity->setName($item['name']);
            $itemEntity->setAmount($item['amount']);
            $manager->persist($itemEntity);
        }

        $manager->flush();
    }
}