<?php 

namespace App\Utils;

class Filter
{
    
    const EQUAL = 'e';
    const NOT_EQUAL = 'ne';
    const GREATER_THAN = 'gt';
    const GREATER_EQUAL = 'ge';
    const LESS_THAN = 'lt';
    const LESS_EQUAL = 'le';
    
    private static $translateTable = [
        'e' => self::EQUAL,
        'ne' => self::NOT_EQUAL,
        'gt' => self::GREATER_THAN,
        'ge' => self::GREATER_EQUAL,
        'lt' => self::LESS_THAN,
        'le' => self::LESS_EQUAL,
    ];
    
    /**
     * @param string $value
     * @return array
     */
    public static function parse(string $value)
    {
        if (strpos($value, ':') === false) {
            return [self::EQUAL, $value];
        }
        
        list($sign, $value) = explode(':', $value, 2);
        
        $sign = self::translateSign($sign);
        if ($sign == null) {
            return null; // można zamiast nulla rzucać wyjątek
        }
        
        return [$sign, $value];
    }
    
    /**
     * @param string $sign
     * @return NULL|string
     */
    private static function translateSign($sign) {
        return self::$translateTable[$sign] ?? null;
    }
}