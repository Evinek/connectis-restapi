<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Item;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    /**
     * @Route("/products", name="products", methods={"GET"})
     */
    public function index(Request $request)
    {
        $amountFilter = $request->get('amount', null);
        // można ewentualnie paginację zrobić, parametr GET page
        
        $items = $this->getDoctrine()
                ->getRepository(Item::class)
                ->findByAmount($amountFilter);

        return $this->json(['items' => $items], Response::HTTP_OK);
    }
    
    /**
     * @Route("/products", name="create_product", methods={"POST"})
     */
    public function create(Request $request)
    {
        
        $name = $request->get('name');
        $amount = $request->get('amount');
        
        if (empty($name)) {
            return $this->json(['error' => 'Missed some parameters. Required parameters: name, amount'], Response::HTTP_BAD_REQUEST);
        }
        if ($amount < 0) {
            return $this->json(['error' => 'Parameter "amount" have to be greater or equal 0'], Response::HTTP_BAD_REQUEST);
        }
        
        $item = new Item();
        $item->setName($name);
        $item->setAmount($amount);
        $em = $this->getDoctrine()->getManager();
        $em->persist($item);
        $em->flush();
        
        return $this->json([$item], Response::HTTP_CREATED);
    }
    
    /**
     * @Route("/products/{id}", name="product", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function one(Request $request, $id)
    {
        
        $item = $this->getDoctrine()
                ->getRepository(Item::class)
                ->find($id);
        
        if ($item === null) {
            return $this->json(['error' => 'The product does not exist'], Response::HTTP_NOT_FOUND);
        }
        
        return $this->json($item, Response::HTTP_OK);
    }
    
    /**
     * @Route("/products/{id}", name="product_update", methods={"PUT"}, requirements={"id"="\d+"})
     */
    public function edit(Request $request, $id)
    {
        $name = $request->get('name');
        $amount = (int)$request->get('amount');
        if ($amount !== null && $amount < 0) {
            return $this->json(['error' => 'Parameter "amount" have to be greater or equal 0'], Response::HTTP_BAD_REQUEST);
        }
        
        $item = $this->getDoctrine()
                ->getRepository(Item::class)
                ->find($id);
        
        if ($item === null) {
            return $this->json(['error' => 'The product does not exist'], Response::HTTP_NOT_FOUND);
        }
        
        if ($name !== null) {
            $item->setName($name);
        }
        if ($amount !== null) {
            $item->setAmount($amount);
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        
        return $this->json(['item' => $item], Response::HTTP_OK);
    }
    
    /**
     * @Route("/products/{id}", name="product_delete", methods={"DELETE"}, requirements={"id"="\d+"})
     */
    public function delete(Request $request, $id)
    {
        
        $item = $this->getDoctrine()
                ->getRepository(Item::class)
                ->find($id);
        
        if ($item === null) {
            return $this->json(['error' => 'The product does not exist'], Response::HTTP_NOT_FOUND);
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        
        return $this->json([], Response::HTTP_OK);
    }
}
